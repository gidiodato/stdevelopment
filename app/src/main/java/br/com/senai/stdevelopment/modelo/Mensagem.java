package br.com.senai.stdevelopment.modelo;

import java.io.Serializable;

/**
 * Created by 36315014801 on 09/03/2018.
 */

public class Mensagem implements Serializable {

    private Long id;
    private String nome;
    private String mensagem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    @Override
    public String toString() {
        return getId()+" - "+getNome()+" - "+ getMensagem();
    }
}
