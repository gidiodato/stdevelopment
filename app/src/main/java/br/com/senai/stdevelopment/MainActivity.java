package br.com.senai.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button botaoLogin;
    private Button botaoQuemSomos;
    private Button botaoNossaHistoria;
    private Button botaoFaleConosco;
    private Button botaoLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoLogin = findViewById(R.id.btnTelaLogin);
        botaoQuemSomos = findViewById(R.id.btnQuemSomos);
        botaoNossaHistoria = findViewById(R.id.btnNossaHistoria);
        botaoFaleConosco = findViewById(R.id.btnFaleConosco);
        botaoLista = findViewById(R.id.btnLista);


        botaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TelaLogin.class);
                startActivity(intent);
            }
        });

        botaoQuemSomos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TelaQuemSomos.class);
                startActivity(intent);
            }
        });

        botaoNossaHistoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TelaNossaHistoria.class);
                startActivity(intent);
            }
        });

        botaoFaleConosco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TelaFaleConosco.class);
                startActivity(intent);
            }
        });

        botaoLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TelaLista.class);
                startActivity(intent);
            }
        });
    }
}
