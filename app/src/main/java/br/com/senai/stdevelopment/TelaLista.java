package br.com.senai.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import br.com.senai.stdevelopment.dao.MensagemDAO;
import br.com.senai.stdevelopment.modelo.Mensagem;

public class TelaLista extends AppCompatActivity {

    private ListView listaDeMensagens;
    private Button botaoMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_lista);

        listaDeMensagens = findViewById(R.id.listaMensagens);
        botaoMenu = findViewById(R.id.btnMenu);

        listaDeMensagens.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                Mensagem mensagem = (Mensagem) listaDeMensagens.getItemAtPosition(position);
                Intent intent = new Intent(TelaLista.this, TelaFaleConosco.class);
                intent.putExtra("mensagem", mensagem);
                startActivity(intent);
                //  Toast.makeText(getApplicationContext(),"Contato: "+contato.getNome()+" Clicado", Toast.LENGTH_LONG).show();
            }
        });

        carregarLista();

        registerForContextMenu(listaDeMensagens);

        botaoMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TelaLista.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }//Fim onCreate

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        MenuItem deletar =  menu.add("Deletar");

        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                Mensagem mensagem = (Mensagem) listaDeMensagens.getItemAtPosition(info.position);

                MensagemDAO dao = new MensagemDAO(TelaLista.this);
                dao.deletar(mensagem);
                dao.close();
                carregarLista();
                //Toast.makeText(getApplicationContext(),"Contato: "+contato.getNome()+" Removido", Toast.LENGTH_LONG).show();
                return false;
            }
        });
    }

    private void carregarLista() {
        MensagemDAO dao = new MensagemDAO(this);
        List<Mensagem> mensagens = dao.buscaMensagem();
        ArrayAdapter<Mensagem> adaptador = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,mensagens);
        listaDeMensagens.setAdapter(adaptador);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregarLista();
    }
}
