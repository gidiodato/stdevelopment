package br.com.senai.stdevelopment;

import android.widget.EditText;

import br.com.senai.stdevelopment.modelo.Mensagem;

/**
 * Created by 36315014801 on 09/03/2018.
 */

public class FormularioHelper {

    public EditText campoNomeFaleConosco;
    public EditText campoMensagemFaleConosco;
    public Mensagem mensagem;

    public FormularioHelper(TelaFaleConosco formulario) {
        campoNomeFaleConosco = formulario.findViewById(R.id.editNomeFaleConosco);
        campoMensagemFaleConosco = formulario.findViewById(R.id.editMensagem);
        mensagem = new Mensagem();
    }
    public Mensagem pegaMensagem() {

        mensagem.setNome(campoNomeFaleConosco.getText().toString());
        mensagem.setMensagem(campoMensagemFaleConosco.getText().toString());
        return mensagem;
    }
    public void preecherFormulario(Mensagem mensagem) {
        campoNomeFaleConosco.setText(mensagem.getNome());
        campoMensagemFaleConosco.setText(mensagem.getMensagem());
        this.mensagem = mensagem;
    }
}

