package br.com.senai.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaNossaHistoria extends AppCompatActivity {
    private Button botaoFaleConosco;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_nossa_historia);

        botaoFaleConosco = findViewById(R.id.btnFalaConosco);

        botaoFaleConosco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TelaNossaHistoria.this, TelaFaleConosco.class);
                startActivity(intent);
            }
        });
    }
}
