package br.com.senai.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.senai.stdevelopment.dao.MensagemDAO;
import br.com.senai.stdevelopment.modelo.Mensagem;

public class TelaFaleConosco extends AppCompatActivity {

    public EditText campoNomeFaleConosco;
    //public EditText campoMensagemFaleConosco;
    public Button btnEnviarFaleConosco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_fale_conosco);

        campoNomeFaleConosco = findViewById(R.id.editNomeFaleConosco);
        //campoMensagemFaleConosco = findViewById(R.id.editMensagem);
        btnEnviarFaleConosco = findViewById(R.id.btnEnviar);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String nome = extras.getString("nome");
            campoNomeFaleConosco.setText(nome);
        }

        final FormularioHelper helper = new FormularioHelper(this);

        Intent intent = getIntent();
        Mensagem mensagem = (Mensagem) intent.getSerializableExtra("mensagem");

        if (mensagem != null){
            helper.preecherFormulario(mensagem);
        }

        btnEnviarFaleConosco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Mensagem mensagem = helper.pegaMensagem();
                MensagemDAO dao = new MensagemDAO(TelaFaleConosco.this);
                if (mensagem.getId() != null){
                    dao.alterar(mensagem);
                }else {
                    dao.inserir(mensagem);
                }
                dao.close();
                Toast.makeText(getApplicationContext(),"Mensagem enviada com sucesso!", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(TelaFaleConosco.this, TelaLista.class);
                startActivity(intent);
                finish();

            }
        });

    }
}
