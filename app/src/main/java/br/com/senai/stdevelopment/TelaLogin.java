package br.com.senai.stdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TelaLogin extends AppCompatActivity {
    public EditText campoNome;
    public Button botaoLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_login);

        campoNome = findViewById(R.id.editNomeLogin);
        botaoLogin = findViewById(R.id.btnTelaLogin);

        botaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Bem-Vindo "+ campoNome.getText().toString(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(TelaLogin.this, TelaFaleConosco.class);
                intent.putExtra("nome", campoNome.getText().toString());
                startActivity(intent);
            }
        });
    }
}
